# Général

Ce qui est attendu du candidat :  

* Découpage photoshop  
* Intégration  
* Développement front-end (jeu de cartes)  
* Développement back-end (enregistrement datas utilisateurs)  

## Informations générales

L'application fait 810px de large, il n'est pas nécessaire d'avoir une version mobile ou responsive.

### Page Home

Au clic sur "Je participe", demande de connexion à Facebook (permission : infos publiques).

### Page Jeu

#### Règles du jeu :
Nombre de cartes : 12
Les cartes apparaissent dans un ordre aléatoire.

L'utilisateur clique sur une première carte : elle se retourne (animation en vidéo jointe : "Flip cards.mov").
L'utilisateur clique sur une seconde carte, elle se retourne :  

* C'est la même : Il marque des points, les deux cartes restent visibles  
* Elles sont différentes : Il ne marque pas de points, la carte reste visible une seconde, puis les deux se remettent de dos.  

Une fois que l'utilisateur a trouvé la dernière paire, il a gagné. On affiche le formulaire par dessus.

#### Points : 
100 points par paire
Si deux paires sont trouvées d'affilée, l'utilisateur marque 200 points. Si trois paires, 300 points, etc...
S'il se trompe et retourne deux différentes, le "combo" s'arrête.

#### Design :
Merci d'utiliser la carte fournie dans le dossier plutôt que celle sur la maquette pour le verso (cartes/carte3-01.png).

### Page Jeu avec formulaire :
A l'envoi du formulaire, on enregistre en base de donnée (en plus du nom/prénom etc) :  

* le score de l'utilisateur  
* le nombre de coups (qui s'incrémente à chaque clic sur une carte)  
* le temps passé, son Facebook ID (récupéré avec le Facebook Login)  
* la date/heure de participation  

Bon courage !